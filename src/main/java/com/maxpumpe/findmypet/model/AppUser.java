package com.maxpumpe.findmypet.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.couchbase.client.java.Collection;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity @Data @NoArgsConstructor @AllArgsConstructor
public class AppUser implements Serializable  {
	
	private static final long serialVersionUID = -200663303929353475L;

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotNull
	@Size(min = 2, max = 25)
	@Column(length = 25)
	private String name;
	
	@NotNull
	@Email
	@Column(length = 100, unique = true)
	private String email;
	
	//@JsonIgnore // Eigenschaft wird bei der Ausgabe als JSON ignoriert
	@NotNull
	@Size(min = 6, max = 100)
	@Column(length = 100)
	private String password;
	

	
	@Enumerated(EnumType.STRING)
	private Role role = Role.USER;
	
	
	//@ManyToMany(fetch = FetchType.EAGER)
	//private Collections<AppRole> roles = new ArrayList<>();


//Methods
	
	public AppUser() {}
	public AppUser(Long id, @NotNull @Size(min = 2, max = 25) String name, @NotNull @Email String email,
			@NotNull @Size(min = 6, max = 100) String password, Role role) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = password;
		this.role = role;
	}




	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}
	
	
	public String getPassword() {
		// TODO Auto-generated method stub
		return password;
	}



	public void setPassword(String pass) {
		password = pass;
		
	}



	public Role getRole() {
		// TODO Auto-generated method stub
		return role;
	}



	public String getEmail() {
		// TODO Auto-generated method stub
		return email;
	}
	

	
	
	@Override
	public String toString() {
		return "AppUser [id=" + id + ", name=" + name + ", email=" + email + ", password=" + password + ", role=" + role
				+ "]";
	}
	
}