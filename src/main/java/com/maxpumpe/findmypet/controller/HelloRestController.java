package com.maxpumpe.findmypet.controller;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.maxpumpe.findmypet.model.Role;
import com.maxpumpe.findmypet.model.AppUser;

import io.swagger.annotations.ApiParam;


@RestController @RequestMapping("v1")
public class HelloRestController {
	
	List<AppUser> userx = Arrays.asList(
			new AppUser (10L,"Otto1","otto1@dazo.de","1234567",Role.USER),
			new AppUser (11L,"Otto2","otto2@dazo.de","1234567",Role.USER),
			new AppUser (12L,"Otto3","otto3@dazo.de","1234567",Role.USER)
			);
	
	
    @GetMapping(value = "/fetch/all")
    public List<AppUser> fetchAllUser() {
        return userx;
    }

    @GetMapping(value = "/fetch/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public AppUser fetchUserbyName( @ApiParam(value = "User Name",required = true) @PathVariable(value = "name") String username ) {
    	
    	return  userx.stream().filter(x -> x.getName().equalsIgnoreCase(username)).findFirst().get();
    }
}
