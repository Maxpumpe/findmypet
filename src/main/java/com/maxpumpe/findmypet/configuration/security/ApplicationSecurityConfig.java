package com.maxpumpe.findmypet.configuration.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import javax.crypto.SecretKey;
import com.maxpumpe.findmypet.auth.ApplicationUserService;

import com.maxpumpe.findmypet.jwt.JwtUsernameAndPasswordAuthenticationFilter;

import static com.maxpumpe.findmypet.configuration.security.ApplicationUserRole.*;

import java.util.Arrays;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

	private final PasswordEncoder passwordEncoder;
	private final ApplicationUserService applicationUserService;

	/*
	 * @Autowired public ApplicationSecurityConfig(PasswordEncoder passwordEncoder)
	 * { this.passwordEncoder = passwordEncoder; }
	 */
	@Autowired
	public ApplicationSecurityConfig(PasswordEncoder passwordEncoder, ApplicationUserService applicationUserService) {
		this.passwordEncoder = passwordEncoder;
		this.applicationUserService = applicationUserService;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.cors().and()
	    .csrf().disable()
	    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
	    .and()
        .addFilter(new JwtUsernameAndPasswordAuthenticationFilter(authenticationManager()))

        .authorizeRequests()
        .antMatchers("/", "index", "/css/*", "/js/*").permitAll()
        .antMatchers("/api/**").hasRole(STUDENT.name())
        .anyRequest()
        .authenticated();
		
		
			//form login and Basic Auth part
				// .csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()).and()
				//.csrf().disable().authorizeRequests().antMatchers("/", "/start", "/login", "/css/*", "/js/*")
				//.permitAll().antMatchers("/api/**").hasRole(STUDENT.name())

				// if you prefer @EnableGlobalMethodSecurity(prePostEnabled = true/false) for
				// false then use code below
				// .antMatchers(HttpMethod.DELETE,
				// "/management/api/**").hasAuthority(ApplicationUserPermission.COURSE_WRITE.getPermission())
				// .antMatchers(HttpMethod.POST,
				// "/management/api/**").hasAuthority(ApplicationUserPermission.COURSE_WRITE.getPermission())
				// .antMatchers(HttpMethod.PUT,
				// "/management/api/**").hasAuthority(ApplicationUserPermission.COURSE_WRITE.getPermission())
				// .antMatchers(HttpMethod.GET,"/management/api/**").hasAnyRole(ADMIN.name(),
				// ADMINTRAINEE.name())
				// .antMatchers("/management/api/**").hasAnyRole(ApplicationUserRole.ADMIN.name(),
				// ApplicationUserRole.ADMINTRAINEE.name())
				//.anyRequest().authenticated().and().httpBasic(); // basic Auth
		// .formLogin()//FormLoginConfigurer.loginPage(String)
		// .loginPage("/login")
		;
	}

	/*
	 * //@Override //@Bean protected UserDetailsService userDetailsService() { //
	 * return super.userDetailsService(); UserDetails annaSmithUser =
	 * User.builder().username("annasmith").password(passwordEncoder.encode(
	 * "password")) // .roles(STUDENT.name()) // ROLE_STUDENT
	 * 
	 * .authorities(STUDENT.getGrantedAuthorities()).build();
	 * 
	 * UserDetails lindaUser =
	 * User.builder().username("linda").password(passwordEncoder.encode(
	 * "password123")) // .roles(ADMIN.name()) // ROLE_ADMIN
	 * .authorities(ADMIN.getGrantedAuthorities()).build();
	 * 
	 * UserDetails tomUser =
	 * User.builder().username("tom").password(passwordEncoder.encode("password123")
	 * ) // .roles(ADMINTRAINEE.name()) // ROLE_ADMINTRAINEE
	 * .authorities(ADMINTRAINEE.getGrantedAuthorities()).build();
	 * 
	 * return new InMemoryUserDetailsManager(annaSmithUser, lindaUser, tomUser);
	 * 
	 * }
	 * 
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(daoAuthenticationProvider());
	}

	@Bean
	public DaoAuthenticationProvider daoAuthenticationProvider() {
		DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
		provider.setPasswordEncoder(passwordEncoder);
		provider.setUserDetailsService(applicationUserService);
		return provider;
	}

	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
	    final CorsConfiguration config = new CorsConfiguration();

	    config.setAllowedOrigins(Arrays.asList("http://localhost:4200","http://qsolog.de:3001"));
	    config.setAllowedMethods(Arrays.asList("GET", "POST", "OPTIONS", "DELETE", "PUT", "PATCH"));
	    config.setAllowCredentials(true);
	    config.setAllowedHeaders(Arrays.asList("Authorization", "Cache-Control", "Content-Type"));

	    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	    source.registerCorsConfiguration("/**", config);

	    return source;
	}
	
}
