package com.maxpumpe.findmypet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.maxpumpe.findmypet.configuration.AppConfig;
import com.maxpumpe.findmypet.model.AppUser;
import com.maxpumpe.findmypet.model.Role;
//import com.maxpumpe.findmypet.service.UserService;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
public class FindmypetApplication implements CommandLineRunner {

	  private static final Logger LOGGER = LoggerFactory.getLogger(FindmypetApplication.class);
	
	@Autowired
	private AppConfig myConfig;
	
	@Value("${app.greeting}")
	private String greetMsg;
	@Autowired
	//UserService userService;
	public static void main(String[] args) {
		SpringApplication.run(FindmypetApplication.class, args);
		  
	}
	
	/* Create with the Commandrunner an inital Superadmin User
	 * 
	 * 
	 */
	@Override
	public void run(String... args) throws Exception {
		/*TODO: Use Environment Parameter from application.properties */
	    System.out.println("using environment: " + myConfig.getEnvironment());
        System.out.println("name: " + myConfig.getName());
        System.out.println("servers: " + myConfig.getServers());
        /*TODO: implement the AppRole Entity for the superuser */
      //  userService.save(new AppUser(null,"SuperAdmin","admin@dazo.de","superadminpasswd",Role.ADMIN));
	}


}
